<?php

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
trait Hewan
{
    protected $nama,
              $jumlahKaki,
              $keahlian,
              $darah;

    /**
     * Hewan constructor.
     * @param $nama
     * @param $jumlahKaki
     * @param $keahlian
     * @param int $darah
     */
    public function __construct($nama, $jumlahKaki, $keahlian, $darah = 50)
    {
        $this->nama       = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian   = $keahlian;
        $this->darah      = $darah;
    }

    public function atraksi(){
        echo "{$this->nama} sedang {$this->keahlian}";
    }
}
