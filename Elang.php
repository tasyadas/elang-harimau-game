<?php

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
class Elang extends Fight
{
    use Hewan {
        Hewan::__construct as private __HewanConstruct;
    }

    public $namaHarimau;
    public $namaElang;

    /**
     * Elang constructor.
     * @param Harimau $namaHarimau
     * @param $namaElang
     */
    public function __construct(Harimau $namaHarimau, $namaElang)
    {
        parent::__construct(10, 5);
        $this->__HewanConstruct($namaElang, 2, 'terbang tinggi');
        $this->namaElang = $namaElang;
        $this->namaHarimau = new $namaHarimau;
    }

    public function penyerang()
    {
        return $this->namaElang;
    }

    public function target()
    {
        $this->namaHarimau->darah -= ($this->namaHarimau->attackPower / $this->namaHarimau->defencePower);

        return $this->namaHarimau->namaHarimau;
    }

    public function getInfoHewan() {
        $this->atraksi();
        echo "<\br>";
        echo "Jenis Hewan Penyerang: Elang";
        echo "<\br>";
        echo "Nama: {$this->namaElang}";
        echo "<\br>";
        echo "Jumlah Kaki: {$this->jumlahKaki}";
        echo "<\br>";
        echo "Keahlian: {$this->keahlian}";
        echo "<\br>";
        echo "Darah: {$this->darah}";

        $this->penyerang();
        echo "<\br>";
        $this->target();
        echo "<\br>";
        echo "Kekuatan Serangan: {$this->attackPower}";
        echo "<\br>";
        echo "Kekuatan Pertahanan: {$this->defencePower}";
    }
}