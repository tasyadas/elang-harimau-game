<?php

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
class Harimau extends Fight
{
    use Hewan {
        Hewan::__construct as private __HewanConstruct;
    }

    public $namaHarimau;
    public $namaElang;

    /**
     * Harimau constructor.
     * @param Elang $namaElang
     * @param $namaHarimau
     */
    public function __construct(Elang $namaElang, $namaHarimau)
    {
        parent::__construct(7, 8);
        $this->__HewanConstruct($namaHarimau, 4, 'lari cepat');
        $this->namaHarimau = $namaHarimau;
        $this->namaElang = new $namaElang;
    }

    public function penyerang()
    {
        return $this->namaHarimau;
    }

    public function target()
    {
        $this->namaElang->darah -= ($this->namaElang->attackPower / $this->namaElang->defencePower);

        return $this->namaElang->namaElang;
    }

    public function getInfoHewan() {
        $this->atraksi();
        echo "<\br>";
        echo "Jenis Hewan Penyerang: Elang";
        echo "<\br>";
        echo "Nama: {$this->namaHarimau}";
        echo "<\br>";
        echo "Jumlah Kaki: {$this->jumlahKaki}";
        echo "<\br>";
        echo "Keahlian: {$this->keahlian}";
        echo "<\br>";
        echo "Darah: {$this->darah}";

        $this->penyerang();
        echo "<\br>";
        $this->target();
        echo "<\br>";
        echo "Kekuatan Serangan: {$this->attackPower}";
        echo "<\br>";
        echo "Kekuatan Pertahanan: {$this->defencePower}";
    }
}