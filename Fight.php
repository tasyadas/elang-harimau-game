<?php

/**
 * @author Tashya Dwi Askara Siahaan <tasyadwiaskarasiahaan@gmail.com>
 **/
abstract class Fight
{
    public $attackPower,
           $defencePower;

    /**
     * Fight constructor.
     * @param $attackPower
     * @param $defencePower
     */
    public function __construct($attackPower, $defencePower)
    {
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    abstract public function penyerang();
    abstract public function target();

    public function serang() {
        echo "{$this->penyerang()} sedang menyerang {$this->target()}";
    }

    public function diserang() {
        echo "{$this->target()} sedang diserang {$this->penyerang()}";
    }
}